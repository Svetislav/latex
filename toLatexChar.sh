#!/bin/bash

info() {
	echo "##########################################################################################################################"
	echo "Ova skripta omogućava automatsku zamenu karaktera đ, Đ, ć, Ć, č, Č, š, Š, ž i Ž odgovarajućim LaTeX simbolima:"
	echo "	đ -> {\leavevmode\setbox0=\hbox{d}\kern0pt\rlap{\kern.215em\raise.46\ht0\hbox{-}}d}"
	echo "	Đ -> {\leavevmode\setbox0=\hbox{D}\kern0pt\rlap{\kern.04em\raise.188\ht0\hbox{-}}D}"
	echo "	ć -> {\' c}"
	echo "	Ć -> {\' C}"
	echo "	č -> {\v c}"
	echo "	Č -> {\v C}"
	echo "	š -> {\v s}"
	echo "	Š -> {\v S}"
	echo "	ž -> {\v z}"
	echo "	Ž -> {\v Z}"
	echo "Skripta prima najviše tri argumenta."
	echo "Prvi argument je tekstualni fajl u kome je potrebno zameniti karaktere."
	echo "Kao drugi (opcioni) argument skripti možemo proslediti opciju '-o' u slučaju da želimo da prepišemo originalni fajl."
	echo "Ukoliko se ne prosledi opcija '-o' biće kreiran novi fajl sa zamenjenim karakterima."
	echo "Treći (opcioni) argument je '-def' kojim umesto uobičajenih (gore pomenutih) LaTeX zamena koristimo nove definicije slova:"
	echo "	đ -> {\dj}"
	echo "	Đ -> {\DJ}"
	echo "	ć -> {\cj}"
	echo "	Ć -> {\CJ}"
	echo "	č -> {\ch}"
	echo "	Č -> {\CH}"
	echo "	š -> {\sh}"
	echo "	Š -> {\SH}"
	echo "	ž -> {\z}"
	echo "	Ž -> {\Z}"
	echo "Ukoliko koristimo treći argument na početku fajla biće ispisane definicije slova koje treba ubaciti u LaTeX fajl."
	echo "##########################################################################################################################"
}

# File's encoding has to be changed in order for character substitution with 'sed' to work, we need 'utf-8' encoding
changeEncoding() {
	encodingOld=$(file -b --mime-encoding "$1")
	if [ $encodingOld = "unknown-8bit" ]
	then
		iconv -f windows-1252 -t utf8 "$1" > temp.txt && mv temp.txt "$1"
	elif [ $encodingOld != "utf-8" ]
	then
		iconv -f $encodingOld -t utf8 "$1" > temp.txt && mv temp.txt "$1"
	fi
}

copyFile() {
	arg1="$1"
	date=$(date +%Y-%b-%d-%Hh%Mm%Ss)
	fileName="${arg1%.*}"
	fileExtension="${arg1##*.}"
	if [ $fileName == $fileExtension ]; then
		tempFile="$fileName-LaTeX-$date"
	else
		tempFile="$fileName-LaTeX-$date.$fileExtension"
	fi
	cp "$1" "$tempFile"
}

changeCaracters1() {
	sed -i -e "s/đ/{\\\leavevmode\\\setbox0=\\\hbox{d}\\\kern0pt\\\rlap{\\\kern.215em\\\raise.46\\\ht0\\\hbox{-}}d}/g" "$1"
	sed -i -e "s/Đ/{\\\leavevmode\\\setbox0=\\\hbox{D}\\\kern0pt\\\rlap{\\\kern.04em\\\raise.188\\\ht0\\\hbox{-}}D}/g" "$1"
	sed -i -e "s/ć/{\\\' c}/g" "$1"
	sed -i -e "s/Ć/{\\\' C}/g" "$1"
	sed -i -e "s/č/{\\\v c}/g" "$1"
	sed -i -e "s/Č/{\\\v C}/g" "$1"
	sed -i -e "s/š/{\\\v s}/g" "$1"
	sed -i -e "s/Š/{\\\v S}/g" "$1"
	sed -i -e "s/ž/{\\\v z}/g" "$1"
	sed -i -e "s/Ž/{\\\v Z}/g" "$1"
}

changeCaracters2() {
	sed -i -e "s/đ/{\\\dj}/g" "$1"
	sed -i -e "s/Đ/{\\\DJ}/g" "$1"
	sed -i -e "s/ć/{\\\cj}/g" "$1"
	sed -i -e "s/Ć/{\\\CJ}/g" "$1"
	sed -i -e "s/č/{\\\ch}/g" "$1"
	sed -i -e "s/Č/{\\\CH}/g" "$1"
	sed -i -e "s/š/{\\\sh}/g" "$1"
	sed -i -e "s/Š/{\\\SH}/g" "$1"
	sed -i -e "s/ž/{\\\z}/g" "$1"
	sed -i -e "s/Ž/{\\\Z}/g" "$1"
}

appendText() {
	echo "#####################################################
Definicije slova:
\def\DJ{\leavevmode\setbox0=\hbox{D}\kern0pt \rlap{\kern.04em\raise.188\ht0\hbox{-}}D}
\def\dj{\leavevmode\setbox0=\hbox{d}\kern0pt \rlap{\kern.215em\raise.46\ht0\hbox{-}}d}
\def\SH{\v S} \def\sh{\v s}
\def\CJ{\' C} \def\cj{\' c}
\def\CH{\v C} \def\ch{\v c}
\def\Z{\v Z} \def\z{\v z}
#####################################################
" | cat - "$1" > temp && mv temp "$1"
}

report1() {
	echo "	đ -> {\leavevmode\setbox0=\hbox{d}\kern0pt\rlap{\kern.215em\raise.46\ht0\hbox{-}}d}"
	echo "	Đ -> {\leavevmode\setbox0=\hbox{D}\kern0pt\rlap{\kern.04em\raise.188\ht0\hbox{-}}D}"
	echo "	ć -> {\' c}"
	echo "	Ć -> {\' C}"
	echo "	č -> {\v c}"
	echo "	Č -> {\v C}"
	echo "	š -> {\v s}"
	echo "	Š -> {\v S}"
	echo "	ž -> {\v z}"
	echo "	Ž -> {\v Z}"
}

report2() {
	echo "	đ -> {\dj}"
	echo "	Đ -> {\DJ}"
	echo "	ć -> {\cj}"
	echo "	Ć -> {\CJ}"
	echo "	č -> {\ch}"
	echo "	Č -> {\CH}"
	echo "	š -> {\sh}"
	echo "	Š -> {\SH}"
	echo "	ž -> {\z}"
	echo "	Ž -> {\Z}"
}

if [ $# -eq 0 ]; then
	info
elif [ $# -eq 1 ]; then
	if [ -f "$1" ]; then
		copyFile "$1"
		if [ $? -ne 0 ]; then
			echo "Došlo je do greške. Zamena karaktera u fajlu '"$1"' nije moguća."
			return 1
		fi
		changeEncoding "$tempFile"
		changeCaracters1 "$tempFile"
		echo "Napravljena kopija '$1' ---> '"$tempFile"' u kojoj su izvršene sledeće zamene karaktera:"
		report1 "$1" "$tempFile"
	else
		echo "Fajl '$1' nije pronađen!"
		return 1
	fi
elif [ $# -eq 2 ]; then
	if [ ! -f "$1" ]; then
		echo "Fajl '$1' nije pronađen!"
		return 1
	fi
	if [ "$2" == "-o" ]; then
		changeEncoding "$1"
		changeCaracters1 "$1"
		echo "U fajlu '$1' su izvršene sledeće zamene karaktera:"
		report1
	elif [ "$2" == "-def" ]; then
		copyFile "$1"
		if [ $? -ne 0 ]; then
			echo "Došlo je do greške. Zamena karaktera u fajlu '"$1"' nije moguća."
			return 1
		fi
		changeEncoding "$tempFile"
		changeCaracters2 "$tempFile"

		appendText "$tempFile"

		echo "Napravljena kopija '$1' ---> '"$tempFile"' u kojoj su izvršene sledeće zamene karaktera:"
		report2
	else
		echo "Nepoznata opcija '"$2"'. Pokrenite 'toLatexChar.sh' bez argumenata za uputstvo o korišćenju skripte."
	fi
elif [ $# -eq 3 ]; then
	if [ ! -f "$1" ]; then
		echo "Fajl '$1' nije pronađen!"
		return 1
	fi
	if [ "$2" == "-o" -a "$3" == "-def" ] || [ "$2" == "-def" -a "$3" == "-o" ]; then
		changeEncoding "$1"
		changeCaracters2 "$1"

		appendText "$1"

		echo "U fajlu '$1' su izvršene sledeće zamene karaktera:"
		report2
	else
		echo "Nepoznata opcija '"$2"' ili '"$3"'. Pokrenite 'toLatexChar.sh' bez argumenata za uputstvo o korišćenju skripte."
	fi
else
	echo "Previše argumenata. Pokrenite 'toLatexChar.sh' bez argumenata za uputstvo o korišćenju skripte."
fi
