# toLatexChar

> `toLatexChar` skripta omogućava automatsku zamenu karaktera đ, Đ, ć, Ć, č, Č, š, Š, ž i Ž odgovarajućim LaTeX kodovima.

Osnovni LaTeX kodovi:

đ -> {\leavevmode\setbox0=\hbox{d}\kern0pt\rlap{\kern.215em\raise.46\ht0\hbox{-}}d}

Đ -> {\leavevmode\setbox0=\hbox{D}\kern0pt\rlap{\kern.04em\raise.188\ht0\hbox{-}}D}

ć -> {\' c}

Ć -> {\' C}

č -> {\v c}

Č -> {\v C}

š -> {\v s}

Š -> {\v S}

ž -> {\v z}

Ž -> {\v Z}

Skripta prima najviše tri argumenta.

Prvi argument je tekstualni fajl u kome je potrebno zameniti karaktere.

Kao drugi (opcioni) argument skripti možemo proslediti opciju '-o' u slučaju da želimo da prepišemo originalni fajl. Ukoliko se ne prosledi opcija '-o' biće kreiran novi fajl sa zamenjenim karakterima.

Treći (opcioni) argument je '-def' kojim umesto uobičajenih (gore pomenutih) LaTeX zamena koristimo nove definicije slova:

đ -> {\dj}

Đ -> {\DJ}

ć -> {\cj}

Ć -> {\CJ}

č -> {\ch}

Č -> {\CH}

š -> {\sh}

Š -> {\SH}

ž -> {\z}

Ž -> {\Z}

Ukoliko koristimo treći argument na početku fajla biće ispisane definicije slova koje treba ubaciti u LaTeX fajl.
